# Arch linux ARM for Orange Pi R1 Plus LTS

Orange Pi R1 Plus LTS is an open-source single-board computer with Dual Gigabit Ethernet ports. It is highly compact with a dimension of 56X57mm.

Orange Pi R1 Plus LTS uses the Rockchip RK3328 SoC, Quad-Core ARM Cortex-A53 64-Bit Processor, Main Frequency Speeds Up To 1.5GHz and 1GB LPDDR3 SDRAM. It integrates cooling fan connector, USB 2.0 port, TF card slot, 13pin headers etc. It is powered through Type-C. It is suitable for industrial control needs.

Features:
* 1GB LPDDR3 SDRAM
* Micro SD
* 1x USB 2.0
* 2x Gigabit ethernet

## Known issues
* Mac addresses are different from the label.

## Installation

> THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
> EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
> MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
> IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
> OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
> ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
> OTHER DEALINGS IN THE SOFTWARE.

Replace *sdX* in the following instructions with the device name for the SD card as it appears on your computer.

1. Zero the beginning of the SD card:
```
dd if=/dev/zero of=/dev/sdX bs=1M count=32
```

2. Start fdisk to partition the SD card:
```
fdisk /dev/sdX
```

3. At the fdisk prompt, create the new partition:
 - a. Type ***o***. This will clear out any partitions on the drive.
 - b. Type ***p*** to list partitions. There should be no partitions left.
 - c. Type ***n***, then ***p*** for primary, 1 for the first partition on the drive, 32768 for the first sector, and then press ENTER to accept the default last sector.
 - d. Write the partition table and exit by typing ***w***.

4. Create the ext4 filesystem:
```
mkfs.ext4 /dev/sdX1
``` 

5. Mount the filesystem:
```
mkdir root
mount /dev/sdX1 root
```

6. Download and extract the root filesystem (as root, not via sudo):
```
wget http://os.archlinuxarm.org/os/ArchLinuxARM-aarch64-latest.tar.gz
bsdtar -xpf ArchLinuxARM-aarch64-latest.tar.gz -C root
```

7. Download the boot.scr script from codeberg for U-Boot and place it in the /boot directory (you can find it in the releases):
```
wget CODEBRG_BOOTSCR_URL -O root/boot/boot.scr
```

8. Add additional device tree file (which is located at dtbs/rockchip64/rk3328-orangepi-r1-plus-lts.dtb in the repository):
```
cp arch-linux-orange-pi-r1-plus-lts/dtbs/rockchip64/rk3328-orangepi-r1-plus-lts.dtb root/boot/dtbs/rockchip64
```


9. Unmount the partition:
```
umount root
```

910 Download and install the U-Boot bootloader (also in the releases):
```
dd if=rksd_loader.img of=/dev/sdX seek=64 conv=notrunc
dd if=u-boot.itb of=/dev/sdX seek=16384 conv=notrunc
```

11. Insert the micro SD card into the Rock64, connect ethernet, and apply 5V power.

12. Use the serial console with a baudrate of 1500000 or SSH to the IP address given to the board by your router.
 - Login as the default user alarm with the password alarm.
 - The default root password is root.
 - The video output is not yet supported.

13. Initialize the pacman keyring and populate the Arch Linux ARM package signing keys:
```
pacman-key --init
pacman-key --populate archlinuxarm
```

14. After the installation, install the u-boot package (in the releases):
```
rm /boot/boot.scr
pacman -U uboot-orangepi-r1-plus-lts-2021.07-1-aarch64.pkg.tar.xz
```
When prompted, press y and hit enter to write the latest bootloader to the micro SD card.

