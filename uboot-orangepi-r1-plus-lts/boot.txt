# After modifying, run ./mkscr
setenv fdtfile "rockchip/rk3328-orangepi-r1-plus-lts.dtb"

part uuid ${devtype} ${devnum}:${bootpart} uuid
setenv bootargs "root=PARTUUID=${uuid} rw rootwait console=ttyS2,1500000"

if load ${devtype} ${devnum}:${bootpart} ${kernel_addr_r} /boot/Image; then
  if load ${devtype} ${devnum}:${bootpart} ${fdt_addr_r} /boot/dtbs/${fdtfile}; then
    if load ${devtype} ${devnum}:${bootpart} ${ramdisk_addr_r} /boot/initramfs-linux.img; then
      booti ${kernel_addr_r} ${ramdisk_addr_r}:${filesize} ${fdt_addr_r};
    else
      booti ${kernel_addr_r} - ${fdt_addr_r};
    fi;
  fi;
fi
